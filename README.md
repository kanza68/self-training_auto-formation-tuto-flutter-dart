# Self-training_Auto-formation - Tuto Flutter Dart

 - [Français](#item-one)
 - [English](#item-two)

 <a id="item-one"></a>
 ## Français

### Introduction
Ce projet est basé sur un tutoriel.

Les technologie utilisées sont :
- Flutter
- Dart
- Android Studio 

### Objectif de ce projet
Ce projet fait partie de mon objectif de maintien et d'amélioration de mes compétence pour cette année 2024.

### Résultats finaux

[Lien vers captures d'écran](https://gitlab.com/kanza68/self-training_auto-formation-tuto-flutter-dart#screenshots)


### Sources
Ce projet se base sur un [tutoriel pour débutant sur Flutter](https://youtu.be/C-fKAzdTrLU?si=x3rUU7Wv64FGZ50Y) publié par Flutter Mapp sur Youtube



<a id="item-two"></a>
## English

### About the project
This project is based on a tutorial

The tools used are :
- Flutter
- Dart
- Android Studio 

### Goal of this project
This project is part of my objective to maintain and improve my skills for 2024.

### Finals results

[Link to screenshots](https://gitlab.com/kanza68/self-training_auto-formation-tuto-flutter-dart#screenshots)

### Sources
For this project I use a [Flutter Tutorial For Beginners](https://youtu.be/C-fKAzdTrLU?si=x3rUU7Wv64FGZ50Y) published by Flutter Mapp on YouTube

___

### Screenshots
![Screenshot 1](./screenshots/Capture_d_ecran_tuto_flutter_dart_1.png)
![Screenshot 2](./screenshots/Capture_d_ecran_tuto_flutter_dart_2.png)
![Screenshot 3](./screenshots/Capture_d_ecran_tuto_flutter_dart_3.png)