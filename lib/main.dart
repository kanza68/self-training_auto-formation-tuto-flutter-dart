import 'package:flutter/material.dart';
import 'package:flutter_tuto_beginners/home_page.dart';
import 'package:flutter_tuto_beginners/profile_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // * MaterialApp for the theme for the entire app
    return MaterialApp(
      debugShowCheckedModeBanner: false, // * To remove the red debug banner on the top screen
      theme: ThemeData(
        focusColor: Colors.blue
      ),
      home: const RootPage(),
    );
  }
}

class RootPage extends StatefulWidget{
  const RootPage({super.key});

  @override
  State<StatefulWidget> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int currentPage = 0; // * Put it before the build, build refresh the value so it will always be false

  List<Widget> pages = const [
    HomePage(),
    ProfilePage()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // *  The general structure is "APP BAR" | "BODY" | "NAVIGATION BAR"
        appBar: AppBar(
          title: const Text('Flutter'),
        ),
      body: pages[currentPage],
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          debugPrint('Floating Action Button'); // * To write on terminal
        },
        child: const Icon(Icons.add)),
      bottomNavigationBar: NavigationBar(destinations: const [
        NavigationDestination(icon: Icon(Icons.home), label: 'Home'),
        NavigationDestination(icon: Icon(Icons.person), label: 'Profile')
      ],
      onDestinationSelected: (int index){
        setState(() { //* To refresh the screen
          currentPage = index;
        });
      },
        selectedIndex: currentPage,
      ),
    );
  }
}