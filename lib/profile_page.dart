import 'package:flutter/material.dart';

const int itemCount = 20;

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {

    return ListView.builder(
      //scrollDirection: Axis.vertical, // The ListView is already scrollable, because it's a list
      itemCount: itemCount,
      itemBuilder: (BuildContext context, int index){
      return ListTile(
        // Remove "const" because of "${}"
          title: Text('Item ${(index+1)}'),
        leading: const Icon(Icons.person),
        trailing: const Icon(Icons.select_all),
        onTap: (){
            debugPrint('Item ${(index+1)} selected');
        }
      );
    },
    );
  }
}