import 'package:flutter/material.dart';

class LearnFlutterPage extends StatefulWidget {
  const LearnFlutterPage({super.key});

  @override
  State<LearnFlutterPage> createState() => _LearnFlutterPageState();
}

class _LearnFlutterPageState extends State<LearnFlutterPage> {
  bool isSwitch = false; // * Put it before the build, build refresh the value so it will always be false
  bool? isCheckbox = false; // * type? => nullable variable

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text('Learn Flutter'),
            automaticallyImplyLeading: false, // * remove default leading
            leading: IconButton(
              onPressed: () {
                Navigator.of(context)
                    .pop(); // Navigate from page to page | pop = delete the page over this one
              },
              icon: const Icon(Icons.arrow_back),
            ),
          actions: [IconButton(onPressed: (){debugPrint('Actions');}
          , icon: const Icon(Icons.info_outline))],
        ),
        body: SingleChildScrollView( // MAKE THE APP SCROLLABLE
          child: Column(children: [
              // (Image.asset) To make this work --> Change pubspec.yaml --> change Asset
              Image.asset('images/app_image.jpg'),
          const SizedBox(
            height: 10,
          ),
          const Divider(
            color: Colors.black,
          ),
          Container(
            // Spacing around the cointainer
            margin: const EdgeInsets.all(10.0),
            // Spacing inside the container
            padding: const EdgeInsets.all(10.0),
            color: Colors.grey,
            // (double.infinity) Take all the place possible
            width: double.infinity,
            child: const Center(
              child: Text(
                'This is a text widget',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              // ? => if the value is true ":" => otherwise do that
              // Here change color based on the value of "isSwitch" change by the Switch element
              backgroundColor: isSwitch ? Colors.green : Colors.blue,
            ),
            onPressed: () {
              debugPrint('Elevating Button');
            },
            child: const Text('Elevated Button'),
          ),
          OutlinedButton(
            onPressed: () {
              debugPrint('Outlined Button');
            },
            child: const Text('Outlined Button'),
          ),
          TextButton(
            onPressed: () {
              debugPrint('Text Button');
            },
            child: const Text('Text Button'),
          ),
          GestureDetector(
            // Opaque to have a result on the entire Row not just children elements
              behavior: HitTestBehavior.opaque,
              onTap: () {
                debugPrint('This is a Row');
              },
              child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.local_fire_department,
                      color: Colors.blue,
                    ),
                    Text('Row widget'),
                    Icon(
                      Icons.local_fire_department,
                      color: Colors.blue,
                    )
                  ])
          ),
          Switch(value: isSwitch, onChanged: (bool newBool) {
            setState(() {
              isSwitch = newBool;
            });
          }),
          Checkbox(value: isCheckbox,
              onChanged: (bool? newBool) { // bool? Nullable bool
                setState(() {
                  isCheckbox = newBool;
                });
              }),
          Image.network(
              'https://www.myaphotography.fr/wp-content/uploads/2015/12/application-photographes.jpg'),
        ]),
        )
    );
  }
}
